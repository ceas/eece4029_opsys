#!/bin/sh
# remove old compiled file
echo "removing previously compiled output"
rm HW2Part2.o
# compile HW2Part1 file
echo "BEGIN COMPILE"
gcc -std=c99 -pedantic -W -Wall HW2Part2.c -o HW2Part2.o
echo "END COMPILE"
# run compiled HW2Part1
./HW2Part2.o
