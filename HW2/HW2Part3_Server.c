/*

Homework 2 Part 3 Client
EECE4029 Operating Systems
Tanner Bornemann
Feb 16, 2019

(3)  Create a server program and a client program.   The client program reads a line from a  text file, converts everything to upper case letters, and send it to the server, using a named pipe. It pauses for 1 second, and repeats the above process, until the end of file.

The client sends a "Stop" string at the end.

     The server displays any thing it receives. The server quits when it sees "Stop". The server needs to cleanup by deleting the named pipe.

Your programs  (client and server) need to create the name pipe first, if it does not exist.

*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

// macros
#define UNUSED(x) (void)(x)

// prototypes
int mkfifo(const char *pathname, mode_t mode);
FILE *popen(const char *command, const char *type);
int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, size_t n);

int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("BEGIN %s - Tanner Bornemann\n", argv[0]);

    sleep(1); // give the client time to start and run

    char *fifoname = "/HW2Part3_pipe";

    char read_msg[256];
    int fd = open(fifoname, O_RDONLY); // listen on named pipe. note: hangs until other side of pipe is opened

    read(fd, read_msg, sizeof(read_msg));
    while (strncmp(read_msg, "Stop", 4) != 0)
    {
        printf("server: %s", read_msg);
        read(fd, read_msg, sizeof(read_msg));
        fflush(stdout);
    }

    if (remove(fifoname) != 0)
    {
        perror("Failed to clean up named pipe");
    }

    close(fd);
    printf("\nEND %s\n", argv[0]);
    fflush(stdout); // cleanup
}