/*

Homework 2 Part 4 Client
EECE4029 Operating Systems
Tanner Bornemann
Feb 20, 2019

(4)  Re-implement Q3, using SysV shared memory. 

Here the difficult is that the client needs to find a way to let the server knows that its have written something to the shared buffer.  You cannot just modify the buffer and hope the server will find out --- what will happen if the new line of text is identical to the contents already in the buffer? (I want you to test this by addling two identical lines in the testing file)

In the examples I showed you in the class, I used sleep(), but clearly it is not an elegant way to things and it may fail.

I suggest you reserve an area in the shared memory section (or create a new shared memory section) to hold an integer.  Each time AFTER the client updates the text buffer, its increments this value. Server will notice the change.

*/

#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
// #include <sys/mman.h>
#include <unistd.h>


// macros
#define UNUSED(x) (void)(x)

// prototypes
int shm_open(const char *name, int oflag, mode_t mode);
unsigned int sleep(unsigned int seconds);
int shm_unlink(const char *name);


int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("BEGIN %s - Tanner Bornemann\n", argv[0]);

    int sharedmemory_fd; // shared memory file descriptor
    sharedmemory_fd = shm_open("memory", O_CREAT | O_RDWR, 0666);
    // int update_fd; // shared memory for updating server
    // update_fd = shm_open("update", O_CREAT | O_RDWR, 0666);

    // printf("client shared memory fd: %i\n", sharedmemory_fd);
    // printf("client update fd: %i\n", update_fd);

    FILE *filepipe = fopen("HW2Part3_Input.txt", "r"); // input file
    
    char line[256];
    // char updatebuff[256];
    // int update = 0;
    
    while (fgets(line, sizeof(line), filepipe))
    {
        char *s = line;
        while (*s)
        {
            *s = toupper((unsigned char)*s);
            s++;
        }
        write(sharedmemory_fd, line, sizeof(line));
        // sprintf(updatebuff, update);
        // write(update_fd, updatebuff, sizeof(updatebuff));
        // printf("client updated: %i\n", update);
        // fputs(line, sharedmemory_fd);
        fflush(stdout);
        sleep(1); // pause 1 sec before talking to server again
        // update++;
    }

    write(sharedmemory_fd, "Stop", 4);

    shm_unlink("memory");
    shm_unlink("update");

    printf("\nEND %s\n", argv[0]);
    fflush(stdout); // cleanup
}