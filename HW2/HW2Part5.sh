#!/bin/sh
# remove old compiled file
echo "removing previously compiled output"
rm HW2Part5_Client.o
rm HW2Part5_Server.o
# compile file
echo "BEGIN COMPILE"
gcc -std=gnu99 -W -Wall HW2Part5_Server.c -o HW2Part5_Server.o
gcc -std=gnu99 -W -Wall HW2Part5_Client.c -o HW2Part5_Client.o
echo "END COMPILE"
echo ""
# run compiled make sure client is first so server has something to do when it starts
./HW2Part5_Client.o &
./HW2Part5_Server.o
