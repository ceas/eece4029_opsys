/*

Homework 2 Part 5 Server
EECE4029 Operating Systems
Tanner Bornemann
Feb 20, 2019

(5)  Re-implement Q4, using mmap() to create a shared memory area between the client and the server. 

*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

// macros
#define UNUSED(x) (void)(x)

const int MAP_SIZE = 1024;
const char *FILE_NAME = "HW2Part5_Data.dat";

int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("BEGIN %s - Tanner Bornemann\n", argv[0]);

    char last_flag = 1;

    // open data file for mapping
    int fd = open(FILE_NAME, O_RDWR | O_CREAT, 0644);

    // seek to last byte
    lseek(fd, MAP_SIZE - 1, SEEK_SET);

    // flag
    write(fd, "0", 1);

    // create region map for communication
    char *region = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    // last byte of shared memory is flag for changed values
    region[MAP_SIZE - 1] = last_flag;

    // wait for client to send region
    while (region[MAP_SIZE - 1] == last_flag);

    // read from client
    while (strcmp(region, "Stop"))
    {
        printf("server: %s", region);

        last_flag = region[MAP_SIZE - 1] + 1;
        region[MAP_SIZE - 1] = last_flag;

        while (region[MAP_SIZE - 1] == last_flag);
    }

    close(fd); // cleanup
    remove(FILE_NAME); // cleanup

    printf("\nEND %s\n", argv[0]);
    fflush(stdout); // cleanup
}
