/*

Homework 2 Part 1
EECE4029 Operating Systems
Tanner Bornemann
Feb 15, 2019

(1) Write a multi-processed C program that does the following:

The program takes two command line parameters. The first one is a program name; the second one is a file name. If the user enters a wrong number of parameters, display a “usage” message and exit.

The program creates the child process, using fork(), and run the specified program in the child process, using exec(). You also need to figure out a way to redirect the standard output file of the child process to the file.

For example, if your program name is “myprg1”, then the following command line will run “/bin/ps” as a new process and redirect the output to “myresult.txt”:

     myprg1 /bin/ps   myresult.txt

Check the lecture slides to see how to redirect stdin/stdout (hints: use dup() or similar system calls). 

You can find the full pathname of any unix command by typing “which program_name” in command line.

*/

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

void die(const char *);

int main(int argc, char **argv)
{
    printf("%s - Tanner Bornemann\n", argv[0]);
    if (argc != 3)
    {
        printf("Failed to parse input. \n\tusage: HW2Part1 <program name> <file name>\n");
        return -1;
    }
    else
    {
        // printf("arg1: %s\n", argv[1]);
        // printf("arg2: %s\n", argv[2]);

        int mainpid = getpid();
        int child = fork();
        if (getpid() != mainpid)
        {
            close(1); // close stdout so we can steal it with dup()
            int openfd = 0;
            openfd = open(argv[2], O_WRONLY | O_CREAT); // open file given by arg2 (as writable) and create it if it doesn't exist
            // dup gives us lowest file descriptor, which is now 1 (stdout) since we closed it earlier
            if (dup(openfd) == -1) {
                die("dup()");
            }
            execl(argv[1], "", NULL); // run program given by arg
            printf("\n"); // print new line so the output from execl doesn't print on newest line of terminal (for formatting purposes)
        }
        else
        {
            // wait(NULL);
        }
    }
}

void die(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}
