#!/bin/sh
# remove old compiled file
echo "removing previously compiled output"
rm HW2Part4_Client.o
rm HW2Part4_Server.o
# compile file
echo "BEGIN COMPILE"
gcc -std=c99 -pedantic -W -Wall HW2Part4_Server.c -o HW2Part4_Server.o -lrt
gcc -std=c99 -pedantic -W -Wall HW2Part4_Client.c -o HW2Part4_Client.o -lrt
echo "END COMPILE"
echo ""
# run compiled make sure client is first so server has something to do when it starts
./HW2Part4_Client.o &
./HW2Part4_Server.o
