/*

Homework 2 Part 4 Server
EECE4029 Operating Systems
Tanner Bornemann
Feb 20, 2019

(4)  Re-implement Q3, using SysV shared memory. 

Here the difficult is that the client needs to find a way to let the server knows that its have written something to the shared buffer.  You cannot just modify the buffer and hope the server will find out --- what will happen if the new line of text is identical to the contents already in the buffer? (I want you to test this by addling two identical lines in the testing file)

In the examples I showed you in the class, I used sleep(), but clearly it is not an elegant way to things and it may fail.

I suggest you reserve an area in the shared memory section (or create a new shared memory section) to hold an integer.  Each time AFTER the client updates the text buffer, its increments this value. Server will notice the change.

*/

#define _XOPEN_SOURCE 700

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

// macros
#define UNUSED(x) (void)(x)

// prototypes
int shm_open(const char *name, int oflag, mode_t mode);
int strncmp(const char *s1, const char *s2, size_t n);
int shm_unlink(const char *name);
long int strtol(const char *nptr, char **endptr, int base);
void *memcpy(void *dest, const void *src, size_t n);

int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("BEGIN %s - Tanner Bornemann\n", argv[0]);

    int sharedmemory_fd; // shared memory file descriptor
    sharedmemory_fd = shm_open("memory", O_RDONLY, 0666);
    // int update_fd; // shared memory for updating server
    // update_fd = shm_open("update", O_RDONLY, 0666);

    // printf("server shared memory fd: %i\n", sharedmemory_fd);
    // printf("server update fd: %i\n", update_fd);

    char read_msg[256];
    char last_msg[256];
    // char updatething[256];
    // char *this_update = "0";
    // char *last_update = "-1";
    
    read(sharedmemory_fd, read_msg, sizeof(read_msg));
    while (strncmp(read_msg, "Stop", 4) != 0)
    {
        // if (strncmp(this_update, last_update, 1) != 0)
        // {
        //     printf("server: %s", read_msg);
        //     fflush(stdout);
        //     last_update = this_update;
        // }

        if (strncmp(last_msg, read_msg, 10) != 0) // cheap way to only print when new data comes in
        {
            // printf("update: %s", updatething);
            printf("server: %s", read_msg);
            // printf("last: %s\n", last_msg);
            fflush(stdout);
            memcpy(last_msg, read_msg, 10);
        }

        read(sharedmemory_fd, read_msg, sizeof(read_msg));
        // read(update_fd, updatething, sizeof(updatething));
        // printf("update: %s", this_update);
    }

    shm_unlink("memory");
    shm_unlink("update");

    printf("\nEND %s\n", argv[0]);
    fflush(stdout); // cleanup
}