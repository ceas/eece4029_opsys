#!/bin/sh
# remove old compiled file
rm HW2Part1.o
# compile HW2Part1 file
gcc -std=c99 -pedantic -W -Wall HW2Part1.c -o HW2Part1.o
# run compiled HW2Part1
./HW2Part1.o $1 $2
