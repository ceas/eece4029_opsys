/*

Homework 2 Part 2
EECE4029 Operating Systems
Tanner Bornemann
Feb 16, 2019

(2) Write a program that uses popen() to do the following:

  The program recursively find all the ".c" files under the current directory (you have to search all the subdirectories also). Then display all file names in UPCASE letters.

   Requirements: 

     (a) Create two pipes, using popen() for each one.

     (b) You need to use popen to run "find" command to find names of c files.

     (c) You need to use popen again to run "tr a-z A-Z" command to convert lower case input to upcase output.

     (d)   use find to find files. Check the following:

            https://kb.iu.edu/d/admm

     (e)  If you want to find more about the "tr" command, do some googling or check the following link: 

             https://www.linuxnix.com/16-tr-command-examples-in-linux-unix/

*/

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

// macros
#define UNUSED(x) (void)(x)

// prototypes
FILE *popen(const char *command, const char *type);

int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("%s - Tanner Bornemann\n", argv[0]);
    
    int PATH_MAX = 128;
    char path[PATH_MAX];
    
    FILE *findpipe = popen("find . -name \"*.c\"", "r");
    FILE *trpipe = popen("tr [:lower:] [:upper:]", "w");
    while (fgets(path, PATH_MAX, findpipe) != NULL)
    {
        fputs(path, trpipe);
    }
}