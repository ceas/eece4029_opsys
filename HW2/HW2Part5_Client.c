/*

Homework 2 Part 5 Client
EECE4029 Operating Systems
Tanner Bornemann
Feb 20, 2019

(5)  Re-implement Q4, using mmap() to create a shared memory area between the client and the server. 

*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>

const int MAP_SIZE = 1024;
const char *FILE_NAME = "HW2Part5_Data.dat";

// macros
#define UNUSED(x) (void)(x)

int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("BEGIN %s - Tanner Bornemann\n", argv[0]);

    char last_flag = 0;

    // open data file for mapping
    int fd = open(FILE_NAME, O_RDWR | O_CREAT, 0644);

    // seek to last byte
    lseek(fd, MAP_SIZE - 1, SEEK_SET);
    // flag
    write(fd, "\0", 1);
    
    // create map for communication
    char *region = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    // wait for server to be ready
    while(region[MAP_SIZE - 1] == last_flag);
    last_flag = region[MAP_SIZE - 1];

    // open file to send to server
    FILE *file = fopen("HW2Part3_Input.txt", "r");
    char line[256];
    while (fgets(line, sizeof(line), file))
    {
        char store = region[MAP_SIZE - 1];
        char *s = line;
        while (*s)
        {
            *s = toupper((unsigned char)*s);
            s++;
        }
        strncpy(region, line, MAP_SIZE);

        last_flag = store + 1;
        region[MAP_SIZE - 1] = last_flag;

        sleep(1);

        // wait on server
        while (last_flag == region[MAP_SIZE - 1]);
    }

    strncpy(region, "Stop", MAP_SIZE);
    last_flag = region[MAP_SIZE - 1] + 1;
    region[MAP_SIZE - 1] = last_flag;

    close(fd); // cleanup

    printf("\nEND %s\n", argv[0]);
    fflush(stdout); // cleanup
}
