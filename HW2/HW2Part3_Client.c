/*

Homework 2 Part 3 Client
EECE4029 Operating Systems
Tanner Bornemann
Feb 16, 2019

(3)  Create a server program and a client program.   The client program reads a line from a  text file, converts everything to upper case letters, and send it to the server, using a named pipe. It pauses for 1 second, and repeats the above process, until the end of file.

The client sends a "Stop" string at the end.

     The server displays any thing it receives. The server quits when it sees "Stop". The server needs to cleanup by deleting the named pipe.

Your programs  (client and server) need to create the name pipe first, if it does not exist.

*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

// macros
#define UNUSED(x) (void)(x)

// prototypes
FILE *popen(const char *command, const char *type);
int pclose(FILE *stream);
int mkfifo(const char *pathname, mode_t mode);

int main(int argc, char **argv)
{
    UNUSED(argc); // suppress unused warning in compiler
    printf("BEGIN %s - Tanner Bornemann\n", argv[0]);

    char *fifoname = "/HW2Part3_pipe";

    FILE *filepipe = fopen("HW2Part3_Input.txt", "r");   // input file

    if (mkfifo(fifoname, S_IWUSR) != 0)
    {
        perror("failed to create named pipe");
    }

    FILE *fifo = fopen(fifoname, "w"); // open named pipe to write. note: hangs until other side of pipe is opened

    char line[256];
    while (fgets(line, sizeof(line), filepipe))
    {
        char *s = line;
        while (*s)
        {
            *s = toupper((unsigned char)*s);
            s++;
        }
        fputs(line, fifo);
        fflush(fifo);
        sleep(1); // pause 1 sec before talking to server again
    }

    fputs("Stop", fifo); // tell server this is the end of the file

    printf("\nEND %s\n", argv[0]);
    fflush(stdout); // cleanup

    fclose(fifo);     // cleanup
    fclose(filepipe); // cleanup
}