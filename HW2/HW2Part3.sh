#!/bin/sh
# remove old compiled file
echo "removing previously compiled output"
rm HW2Part3_Client.o
rm HW2Part3_Server.o
# compile file
echo "BEGIN COMPILE"
gcc -std=c99 -pedantic -W -Wall HW2Part3_Server.c -o HW2Part3_Server.o
gcc -std=c99 -pedantic -W -Wall HW2Part3_Client.c -o HW2Part3_Client.o
echo "END COMPILE"
echo ""
# run compiled make sure client is first so server has something to do when it starts
./HW2Part3_Client.o &
./HW2Part3_Server.o
