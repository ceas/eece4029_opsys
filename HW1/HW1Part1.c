/*

Homework 1
EECE4029 Operating Systems
Tanner Bornemann
Jan 26, 2019

(1) Write a Unix program that does the following

      You run the program in command line using the following syntax:

                       you_program_name N

       Where N is a number

      When the program starts, it does the following

      1) If N is not specified in the command line, or the parameter is not a number, or if there are too many parameters, display the correct usage and then exit.

      2) It  forks three (3) child processes

          2a)  The main process then enters N loops that repeatedly displays
                 one line of information like "This is the main process, my PID is ...."
                 then pauses for about 2 seconds, then displays the above information again
          2b)  Each of the three child processes repeatedly displays one line of information
                 like "This is a child process, my PID is ..., my parent PID is ...",
                 then pauses for about 2 seconds, then displays the above information again, for a total of N times.
*/

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    printf("HW1 Part1 - Tanner Bornemann\n");
    if (argc != 2)
    {
        printf("Failed to parse input. \n\tusage: HW1 <integer>\n");
        return -1;
    }
    else
    {
        // printf("input: %s\n", argv[1]);

        int mainPid = getpid();
        // printf("This is the main process, my PID: %i\n", mainPid);
        int child1 = fork();
        if (mainPid == getpid())
        {
            int child2 = fork();
        }
        if (mainPid == getpid())
        {
            int child3 = fork();
        }

        for (int i = 0; i < atoi(argv[1]); i++)
        {
            // printf("Loop: %i | Current PID: %i\n", i, getpid());
            if (mainPid == getpid())
            {
                printf("This is the main process, my PID: %i\n", getpid());
            }
            else
            {
                printf("This is a child process, my PID: %i my parent PID: %i\n", getpid(), mainPid);
            }
            fflush(stdout); // make sure printf prints before sleep()
            sleep(2);
        }
    }
    return 0;
}
