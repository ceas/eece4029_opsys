#!/bin/sh
# remove old compiled file
rm HW1Part1
# compile HW1 file
gcc -std=c99 -pedantic -W -Wall HW1Part1.c -o HW1Part1
# run HW1 output with input of 2
./HW1Part1 2
