/*

Homework 1
EECE4029 Operating Systems
Tanner Bornemann
Jan 26, 2019

(2) Write a Unix program that does the following

      You run the program in command line using the following syntax:

                       you_program_name file_name

       Where file_name is the name of a text file under the current directory

      When the program starts, it does the following

      If filename is not specified in the command line, or if there are too many parameters, display the correct usage and then exit. Otherwise,

      It forks three (3) child processes


      The parent process then displays its own PID information only once, then waits for all its child processes die (hint: use wait() for this.. The wait() system call suspends the calling process until one of its child processes dies.)


      Let one child-process run the "ls -l" command (using the "execl" system call);
      Let another child-process run the "ps -ef" command;
      Let the third child-process display the content of the file (specified by file_name). You can use the program "more" or "cat" to display it.
      After all child processes terminate, the main process displays "main process terminates" then exits.  This should be the last piece of information displayed by your program.
*/

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    printf("HW1 Part2 - Tanner Bornemann\n");
    if (argc != 2)
    {
        printf("Failed to parse input. \n\tusage: HW1 <file_name>\n");
        return -1;
    }
    else
    {
        int mainPid = getpid();
        int child1 = fork();
        int child2 = -1;
        int child3 = -1;
        if (mainPid == getpid())
        {
            child2 = fork();
        }
        if (mainPid == getpid())
        {
            child3 = fork();
        }

        if (child1 == 0)
        {
            execl("/bin/ls", "ls -l", NULL);
        }
        else if (child2 == 0)
        {
            execl("/bin/ps", "ps -ef", NULL);
        }
        else if (child3 == 0)
        {
            execl("/bin/more", "more", "test.txt", NULL);
        }
        else
        {
            wait(NULL); // wait for child 1
            wait(NULL); // wait for child 2
            wait(NULL); // wait for child 3
            printf("\nmain process terminates\n");
        }
    }
}
