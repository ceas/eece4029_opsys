#!/bin/sh
# remove old compiled file
rm HW1Part2
# compile HW1 file
gcc -std=c99 -pedantic -W -Wall HW1Part2.c -o HW1Part2
# run HW1 output
./HW1Part2 test.txt
